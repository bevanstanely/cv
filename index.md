---
layout: cv
title: Bevan Stanely's CV
---
# Bevan Stanely
Biologist transitioning to mathematics and other theoretical sciences to study complex systems

<div id="webaddress">
<a href="mailto:bevanstanely@iisc.ac.in">bevanstanely@iisc.ac.in</a>
| <a href="https://bevanstanely.gitlab.io">My Website</a>
</div>



## Currently

Working on agent based model for the developmental life cycle of #Myxococcus xanthus# a soil bacterium.

### Specialized in




### Research interests

Complex systems, agent based models.


## Education

`August 2019 - now`
Integrated PhD in Biological Sciences
@[Indian Institute of Science, Bengaluru](https://www.iisc.ac.in/)

`2016-2019`
Bachelor of Science in Biological Sciences
@[Bangalore University](https://eng.bangaloreuniversity.ac.in/biological-sciences/)

## Experience

`August 2020`
Mediator, "PHYTOPIA Digital Exhibition", Science Gallery Bengaluru.

`December 2019 - January 2020`
Mediator, "SUBMERGE Exhibition", Science Gallery Bengaluru.

<!-- ### Footer

Last updated: September 2020 -->


